<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ДЗ 2-1</title>
    <style>
        .phone {
            width: 300px;
            background-color: #fff6b6;
            border-radius: 5px;
            padding: 10px;
            margin: 5px;
        }
        .phone h3 {
            font-style: italic;
        }
        table {
            border-collapse: collapse;
            border: 1px solid black;
        }
        th {
            background-color: #fff6b6 ;
            border: 1px solid black;
        }
        td {
            border: 1px dashed black;
            padding: 10px;
        }
        tr:nth-child(odd) {
            background-color: #ebebeb;
        }
        thead th {
            text-transform: uppercase;
            padding: 10px;
            background-color: antiquewhite;
        }

    </style>
</head>
<body>
    <table>
<!--        <caption>Телефонная книга</caption>-->
        <thead>
            <tr>
                <th colspan="3">Телефонная книга</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Имя</th>
                <th>Город</th>
                <th>Телефон</th>
            </tr>
        <?php foreach ($phoneBook as $contacts):?>
            <tr>
                <td><?= $contacts['firstName'] . ' ' . $contacts['lastName']; ?></td>
                <td><?= $contacts['address']; ?></td>
                <td><?= $contacts['phone']; ?></td>
            </tr>
        <?php endforeach;?>
        </tbody>

    </table>
</body>
</html>