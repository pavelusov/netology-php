<?php
// 1 Создаем многомерный массив с животными
$continents = [
    'africa'=> ['varanus ornatus', 'conraua goliath'],
    'antarctica'=> ['aptenodytes forsteri', 'сryolophosaurus ellioti '],
    'asia'=> ['tamias', 'mogera robusta'],
    'europa'=> ['bison bonasus', 'lutra'],
    'australia'=> ['macropus rufus', 'tiliqua multifasciata'],
    'north america'=> ['euceratherium collinum', 'piksi varricchi'],
    'south america'=> ['dendrobates leucomelas', 'galictis']
];
// 2 Ищем зверей из двух зверей и составляем новый массив
$animalTwoWords = [];
foreach ($continents as $continent => $title){
//            echo "<h1>{$continent}</h1>";
    foreach ($title as $animal){
        if (strpos($animal, ' ')){
//                    echo "<p>{$animal}</p>";
//                    echo "<p>{$title}</p>";
            $animalTwoWords[] = $animal;
        }
    }
}
// 3 Перемешиваем
$animalFirstWord = [];
$animalLastWord = [];
$animalExplode = [];
for ($i = 0; $i < count($animalTwoWords); $i++){
    $animalExplode[] = explode(" ", $animalTwoWords[$i]);

}
for ($i = 0; $i < count($animalExplode); $i++) {
    $animalFirstWord[] = $animalExplode[$i][0];
    $animalLastWord[] = $animalExplode[$i][1];
}
shuffle($animalFirstWord);
shuffle($animalLastWord);

$animalFantasy = [];
for ($i = 0; $i < count($animalExplode); $i++) {
    $animalFantasy[] = "{$animalFirstWord[$i]} {$animalLastWord[$i]}";
}

?>

<!doctype html>
<html lang=ru>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>"Строки и массивы."</title>
</head>
<body>

    <h1>Жесткое обращение с животными:</h1>
    <ul>
    <?php
        foreach ($animalFantasy as $a){
        echo "<li>{$a}</li>";
        }
        ?>
    </ul>
</body>
</html>
