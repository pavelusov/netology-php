<?php
require 'imgtocsv.php';
$imageList = [];
// Получаем ресурс
$output = fopen('imgData.csv', 'r');
//Делаем из ресурса массив
while ($view = fgetcsv($output)){
        $imageList[$view[0]] = array_merge([$view[1]], [$view[2]]);
}
fclose($output);
foreach ($imageList as $filename => $v) {

    $pathThumb = 'img/tmb/' . str_replace('.jpg', '_small.jpg', basename($filename));
    $percent = 0.1;
    list($width, $height) = getimagesize($filename);
    $newWidth = $width * $percent;
    $newHeight = $height * $percent;
//Создание нового изображения
    $thumb = imagecreatetruecolor($newWidth, $newHeight);
    $source = imagecreatefromjpeg($filename);
//изменение размера
    imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
    imagejpeg($thumb, $pathThumb);
}



?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hello standart functions!</title>
    <style>
        ul {
            display: flex;
            flex-direction: column;
        }
        li {
            display: block;
            padding: 10px;
            margin: 5px;
            background: #22b5ff;
            width: 200px;
            border-radius: 10px;

        }
        a {
            color: white;
            text-decoration: none;
            padding-left: 30px;
        }

    </style>
</head>
<body>
    <h2>Список файлов:</h2>
    <ul>
        <?php
        foreach ($imageList as $path => $imageData){
            echo "<li><a href='$path'> фото <img src='img/tmb/". str_replace('.jpg', '_small.jpg', basename($path)) . "' alt='photo'></a></li>";
        }
        ?>
    </ul>
    
</body>
</html>

