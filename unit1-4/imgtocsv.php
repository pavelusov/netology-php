<?php
$images = glob('img/*.jpg');
//foreach ($images as $img) {
//    echo '<p>Файл ' . basename($img) . ': размер - ' . ceil(filesize($img)/1024) .
//        ' KB. Время изменения: ' . date("G:i d.m.Y", fileatime($img)) . '</p>';
//}
//var_dump($images);
$listImages = [];
foreach ($images as $num => $img) {
    $listImages[$img]['size'] = filesize($img);
    $listImages[$img]['date'] = fileatime($img);

}
$imgData = fopen('imgData.csv', 'w');
foreach ($listImages as $path => $image) {
    fputcsv($imgData, array_merge([$path], $image));
}
fclose($imgData);