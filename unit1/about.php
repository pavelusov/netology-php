<?php
$name = 'Павел';
$lastname = 'Усов';
$age = 30;
$address = 'Победы 100';
$email = 'usov@mail.ru';
$city = 'Екатеринбург';
$about = 'PHP разработчик';
?>
<!doctype html>
<html lang=ru>
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        div {
            display: table-row;
        }
        div p {
            display: table-cell;
            min-width: 100px;
        }
    </style>
</head>
<body>
    <H1>Пользователь: <?= $name . ' ' . $lastname ?></H1>
    <div>
        <p>Имя:</p>
        <p><?= $name . ' ' . $lastname ?></p>
    </div>
    <div>
        <p>Возраст:</p>
        <p><?= $age ?></p>
    </div>
    <div>
        <p>Адрес:</p>
        <p><?= $address ?></p>
    </div>
    <div>
        <p>E-mail:</p>
        <p><?= $email ?></p>
    </div>
    <div>
        <p>Город:</p>
        <p><?= $city ?></p>
    </div>
    <div>
        <p>О себе:</p>
        <p><?= $about ?></p>
    </div>

</body>
</html>