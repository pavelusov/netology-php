<?php
$message = '';
$text = '';
// Комп задумал число
$a = rand(1, 100);
//$a = 50;  // для теста
// Игрок ввел число
if (isset($_GET['num']) && ( ($_GET['num'] > 0) && ($_GET['num'] <=100) )   ){
    $b = $_GET['num'];
}
// Сравнение значений игрока и компа
//if ($b > $a) {
//    $message = 'МНОГО (Ваше число больше!)';
//}else {
//    if ($b < $a) {
//        $message = 'мало (Ваше число меньше!)';
//    }else {
//        $message = 'Вы угадали! (Ваши числа совпадают!)';
//    }
//}

switch ($b) {
    case $b > $a :
        $message = 'МНОГО (Ваше число: ' . $b . '. У компьютера: '. $a .')';
        $text = 'Попробуйте ввести еще раз!';
        break;
    case $b < $a :
        $message = 'Мало (Ваше число: ' . $b . '. У компьютера: '. $a .')';
        $text = 'Попробуйте ввести еще раз!';
        break;
    default :
        $message = 'Вы угадали! (Ваше число: ' . $b . '. У компьютера: '. $a .')';
        $text = '';


}

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Основы PHP</title>
</head>
<body>
    <section>
        <h1>У кого число больше!</h1>
        <form action="index.php" method="get">
            <label for="num">Игрок введите число (от 1 до 100):</label>
            <input id="num" name="num" type="num">
            <input type="submit">
        </form>
        <?php
            if ( !(empty($b))) {
               echo '<h2 style="color: green">'.$message.'</h2>';
               echo '<h2 style="color: crimson">'.$text.'</h2>';
           }
        ?>
    </section>
</body>
</html>
