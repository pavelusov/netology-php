<?php
error_reporting(E_ALL);
// Получение имя файла json
function getNameJson($sourceName){
    return substr($sourceName, 0, strpos($sourceName, '.json'));
}
// Получение директории
function getListFile($dirname) {
    return scandir($dirname);
}
// Приведение в верхний регистр
function elemUpper ($elem){
    return strtoupper($elem);
}
// Создание сертификата
function createSertificate ($text1, $text2, $name){
    header('Content-type: image/jpg');
    $image = imagecreatetruecolor(700, 350); // полотно
    $bg = imagecolorallocate($image, 11, 133, 161);
    // картинка
    $cupPath = realpath(__DIR__ . '/assets/cup.png');
    $cup = imagecreatefrompng($cupPath);
    // текст
    $fontPath = realpath(__DIR__ . '/assets/aBosaNovaDcFr.ttf');
    $textColor = imagecolorallocate($image, 255, 255, 255);
    $text3 = "Congratulations {$name}!";

    imagefill($image, 0, 0, $bg); // заливаем полотно
    // вставляем картинку в полотно
    imagecopy($image, $cup, 30, 120, 0, 0, 179, 179);
    imagettftext($image, 30, 0, 250, 200, $textColor, $fontPath, $text1);
    imagettftext($image, 30, 0, 250, 265, $textColor, $fontPath, $text2);
    imagettftext($image, 30, 0, 40, 60, $textColor, $fontPath, $text3);

    imagejpeg($image); // Показываем картинку
    imagedestroy($image);
    imagedestroy($cup);

}
// Получение пары логин - пароль
function getLogin($inputPathJson){
    $loginJson = file_get_contents($inputPathJson);
    $getLogin = json_decode($loginJson, true);
    define('LOGIN', $getLogin['login']);
    define('PASS', $getLogin['pass']);
}