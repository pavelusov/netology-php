<?php
session_start();
if(!empty($_SESSION['is_admin']) && isset($_SESSION['is_admin'])){
    session_destroy();
    header('Location: index.php');
    die();
}
echo 'logout';