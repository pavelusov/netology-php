<?php
session_start();

if(!empty($_SESSION['is_admin']) && isset($_SESSION['is_admin'])){
    header('Location: admin.php');

}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Unit 2-2</title>
    <style>
        .btn {
            background-color: #6b027f;
            color: #ffffff;
            padding: 10px 40px;

        }
        .forms {
            width: 400px;
            margin: 50px;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 20px rgb(0, 0, 0);
        }

        .forms input {
            margin-bottom: 15px;
        }
        .m-form-red {
            background-color: #ffdec9;
        }
        .m-form-green {
            background-color: #cdffc5;
        }
        .wrap {
            display: flex;
            justify-content: space-around;
        }
        .center {
            display: flex;
            justify-content: center;
            color: #9a030c;
        }
    </style>
</head>
<body>
   <div class="wrap">
    <div class="forms m-form-red">
        <form action="auth.php" method="post" name="userForm">
            <fieldset>
                <legend>Enter the TEST system:</legend>
                    <label for="label">Login:</label>
                    <input type="text" id="label" name="login">
                    <br>
                    <label for="pass">Password:</label>
                    <input type="password" id="pass" name="pass">
                    <br>
                    <button type="submit">Login</button>
            </fieldset>
        </form>
    </div>
    <div class="forms m-form-green">
        <form action="auth.php" method="post" name="guestForm">
            <fieldset>
                <legend>Take the TEST!</legend>
                <label for="guest">Name:</label>
                <input type="text" id="guest" name="guest">
                <br>
                <button type="submit">Take</button>
            </fieldset>
        </form>
    </div>
   </div>
   <?php if(!empty($_GET['msg']) && isset($_GET['msg'])):?>
       <div class="center">
           <h2><?=$_GET['msg']?></h2>
       </div>
   <?php endif;?>
</body>
</html>