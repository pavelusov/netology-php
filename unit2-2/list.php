<?php
error_reporting(E_ALL);
require "functions.php";
//session_start();
//if(empty($_SESSION['is_admin']) && !isset($_SESSION['is_admin']) || empty($_SESSION['name'])){
//    header('Location: index.php');
//}
//var_dump($_COOKIE);
//session_start();
if(empty($_SESSION['is_admin']) && !isset($_SESSION['is_admin'])){
    if(!empty($_COOKIE['guest_name']) && isset($_COOKIE['guest_name'])){
        $winner = $_COOKIE['guest_name'];
//        session_destroy();
    }else {
        header('Location: index.php');
    }

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>List tests</title>
    <style>
        li {
            margin: 10px;
        }
    </style>
</head>
<body>
<?php
    if ( isset($_GET['text']) ){
     echo "<h1>{$_GET['text']}</h1>";
     echo "<p>Take the test again!</p>";
    }
?>
<h2>List tests:</h2>
<form action="test.php" method="get" enctype="application/x-www-form-urlencoded">
    <ul>
        <?php foreach (getListFile('test') as $test) :?>
            <?php if ( strpos($test, '.json')):?>
                <li><?= strtoupper(getNameJson($test));?>
                    <button type="submit" name="test" value="<?= getNameJson($test);?>">
                        Go run test!
                    </button>
                    <?php if( !empty($_SESSION['is_admin']) && isset($_SESSION['is_admin']) ):?>
                    <button type="submit" name="delete" value="<?= getNameJson($test);?>">
                        Delete test!
                    </button>
                    <?php endif;?>
                </li>
            <?php endif; ?>
        <?php endforeach;?>
    </ul>
</form>
</body>
</html>
<!--!($test === '.' || $test === '..' || $test === '.DS_Store') -->