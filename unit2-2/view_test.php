<?php
error_reporting(E_ALL);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>
    <style>
        .test {
            margin-bottom: 15px;
        }

    </style>
</head>
<body>
    <h1>Your test:</h1>
    <form action="test.php" method="GET">
        <?php foreach ($test as $items):?>
            <div class="test">
                <label for="<?=$items['id']?>"><?= $items['question']?></label>
                <input type="text" id="<?=$items['id']?>" name="<?=$items['id']?>">
            </div>
        <?php endforeach;?>
<!--        <div class="test">-->
<!--            <label for="name">Name</label>-->
<!--            <input type="text" name="name" id="name" placeholder="Enter your name...">-->
<!--        </div>-->
        <button type="submit" name="answer" value="<?=$_GET['test'];?>">Go!</button>
    </form>

</body>
</html>