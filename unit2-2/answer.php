<?php
error_reporting(E_ALL);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Answer</title>
    <style>
        a {
            display: inline-block;
            padding: 10px;
            background-color: antiquewhite;
            border: 1px solid black;
            text-decoration: none;
            border-radius: 5px;
        }
    </style>
</head>
<body>
    <h1>Test</h1>
    <?php foreach ($test as $key => $value):?>
        <?php foreach ($value as $k =>  $v):?>
            <?php if ($k !== 'id'):?>
                <p><?=$k?> : <?=$v?></p>
            <?php endif;?>
        <?php endforeach;?>
    <?php endforeach;?>
    <h1>Your answer</h1>
    <?php foreach ($_GET as $k => $value):?>
            <?php if ($k !== 'answer'):?>
                <p>Your answer: <?=$value?></p>
            <?php endif;?>
    <?php endforeach;?>
    <a href="index.php">Go load JSON file.</a>
</body>
</html>