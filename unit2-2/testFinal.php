<?php
//session_start();
if(empty($_SESSION['is_admin']) && !isset($_SESSION['is_admin'])){
    if(!empty($_COOKIE['guest_name']) && isset($_COOKIE['guest_name'])){
        $winner = $_COOKIE['guest_name'];
        $btnAdmin = false;
    }else {
        header('Location: index.php');
        session_destroy();
    }

}else {
    $btnAdmin = true;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>You passed the test!</title>
    <style>
        .center {
            display: flex;
            flex-direction: column;
            align-items: center;

        }
        .btn {
            width: auto;
            text-transform: uppercase;
            margin: 50px;

        }
        .btn button {
            font-size: 20px;
        }
    </style>
</head>
<body>
    <div class="center">
        <h1><?=$txt?></h1>
        <img src="sertificate.php?answercount=<?=$answerCount?>&questcount=<?=$questCount?>" alt="sertificate">
        <?php if($btnAdmin):?>
            <a class="btn" href="admin.php"><button type="button">back admin page</button></a>
        <?php endif;?>
        <?php if(!$btnAdmin):?>
            <a class="btn" href="index.php"><button type="button">back start page</button></a>
        <?php endif;?>
    </div>
</body>
</html>