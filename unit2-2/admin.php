<?php
error_reporting(E_ALL);
session_start();
if(empty($_SESSION['is_admin']) && !isset($_SESSION['is_admin'])){
    header('Location: index.php');
    die();
}
if (isset($_FILES['jsonFile'])){
    if (!($_FILES['jsonFile']['type'] === "application/json")){
        echo "<p>Download file json format</p>";
        die();
    }
    $sourceFile = $_FILES['jsonFile']['tmp_name'];
    $upload_dir = realpath(__DIR__ . '/test/');
    $name = $_FILES['jsonFile']['name'];
    move_uploaded_file($sourceFile, "$upload_dir/$name");
    header("Location: admin.php");
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <style>
        .btn {
            width: auto;
            text-transform: uppercase;
            margin: 50px;

        }
        .btn button {
            font-size: 20px;
        }
    </style>
</head>
<body>
<h1>Load json-file:</h1>
<form action="admin.php" method="post" enctype="multipart/form-data">
    <input type="file" name="jsonFile">
    <button class="btn" type="submit">Load</button>
</form>
<?php
include 'list.php';
?>
<form action="logout.php" class="btn" method="post">
    <button type="submit" name="logout" value="out">Logout</button>
</form>
</body>
</html>

